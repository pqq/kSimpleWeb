
# ---------------------------------------------
# filename:	kSimpleWeb.pm
# author:	Frode Klevstul (frode@klevstul.com)
# started:	29.10.2004
# version:	v01_20050529
# ---------------------------------------------



# --------------------------------------------------
# package setup
# --------------------------------------------------
package kSimpleWeb;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
	kSimpleWeb_getIniValue
	kSimpleWeb_printPage
);




# -----------------------------------------------
# Global declarations
# -----------------------------------------------



# -----------------------------------------------
# sub rutines
# -----------------------------------------------



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		Opens a file and returns a parameter value.
#			The file has to be on the format:
#			PARAMETER_NAME		PARAMETER_VALUE
#			(name and value has to be separated by tab)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub kSimpleWeb_getIniValue{
	my $parameter 	= $_[0];					# the name of the parameter to what we want it's value
	my $line;
	my @value;									# NB: This procedure returns an array!

	my $ini_file	= "setup/siteconfig.ini";

    open(FILE, "<$ini_file") || die("failed to open '$ini_file'");
	LINE: while ($line = <FILE>){
		if ($line =~ m/^#/){
			next LINE;
		} elsif ($line =~ m/^$parameter(\t)+(.*)$/){
			push(@value,$2);
			if ($stop_value == 1){
				last LINE;
			}
		}	
	}
	close (FILE);
	
	return @value;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints a page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kSimpleWeb_printPage{
	my $type				= $_[0];																# 'top' or 'bottom'
	my $pageSelected		= $_[1];																# file to read from

	my @page_title			= &kSimpleWeb_getIniValue("PAGE_TITLE");
	my @background_color	= &kSimpleWeb_getIniValue("BACKGROUND_COLOR");
	my @linkset 			= &kSimpleWeb_getIniValue("LINKSET");

	my $file;																						# file to read from
	if ($type eq "top"){
		$file = "setup/top.html";
	} elsif ($type eq "bottom"){
		$file = "setup/bottom.html";
	}
	my $img					= $pageSelected;														# background image (index.html has a background called index.jpg)
	$img 					=~ s/\.html/\.jpg/;

	my $line				= undef;
	my $i 					= 0;

	foreach (@linkset){																				# selected is highlighted
		$linkset[$i] =~ s/$pageSelected">/$pageSelected" class="selected">/s;
		$i++;
	}

    open(FILE, "<$file") || die("failed to open '$file'");
	while ($line = <FILE>){
		if ($line =~ m/PAGE_TITLE/){
			$line =~ s/PAGE_TITLE/$page_title[0]/sg;
		}
		if ($line =~ m/BACKGROUND_COLOR/){
			$line =~ s/BACKGROUND_COLOR/$background_color[0]/sg;
		}
		if ($line =~ m/BACKGROUND_IMAGE/){
			$line =~ s/BACKGROUND_IMAGE/$img/sg;
		}
		if ($line =~ m/LINKSET1/){
			$line =~ s/LINKSET1/$linkset[0]/sg;
		}
		if ($line =~ m/LINKSET2/){
			$line =~ s/LINKSET2/$linkset[1]/sg;
		}
		print $line;
	}
	close (FILE);
}

