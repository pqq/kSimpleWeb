#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ---------------------------------------------
# filename:	run.pl
# author:	Frode Klevstul (frode@klevstul.com)
# started:	29.10.2004
# version:	v01_20041029
# ---------------------------------------------

# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
#
# version:  v01_20041030
#           - The first version of kSimpleWeb was developed
# ------------------------------------------------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
use kSimpleWeb;


# ------------------
# declarations
# ------------------
my $file;
my $output		= $file;
my $dir_export	= "export";
my $dir_setup 	= "setup";
my $dir_img 	= "images";
my $dir_trash 	= "trash";


# ------------------
# main
# ------------------
&generate_HtmlFile;


# ------------------
# sub
# ------------------

sub generate_HtmlFile{
	system("move $dir_export\\* $dir_trash\\");

	foreach (@ARGV){
		if ($_ =~ m/\.html/){
			$output = $_;
			$output =~ s/\.cgi/\.html/;

			system("perl createPage.pl $_ > $dir_export\\$output");
		}
	}

	system("copy $dir_setup\\kSimpleWeb.css $dir_export\\kSimpleWeb.css");
	system("copy $dir_img\\* $dir_export\\");
}
